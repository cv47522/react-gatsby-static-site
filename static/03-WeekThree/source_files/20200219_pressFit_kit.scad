// increase rendering resolution
$fa = 1;
$fs = 0.4;

///////////////
//  Base Shape: mm(unit)
///////////////
radius_x = 25;
radius_y = 10;
width = 50 + radius_x * 2;
height = 80 + radius_y * 2;

//////////////////////////////
//  Laser Cut & Material & Chamfer Setting: mm(unit)
//////////////////////////////
laser_offset = 0; 
laser_kerf_width = 0.125 + laser_offset;
sheet_thickness = 6 - laser_kerf_width;
sheet_kerf_length = 50;
chamfer_kerf_width = 10;
chamfer_offset_angle =  -0.5;

combination();

*for(dx = [0: width+radius_x*2 :500]) {
    for(dy = [0: height+radius_y*2 :500]) {
        translate([dx,dy]) combination();
    }
}

module combination() {
    union() {
        difference() {
            base_shape();
            sheet_kerf();
            chamfer_kerf();
        }
    }
}

module chamfer_kerf() {
    translate([-width/2-radius_x, 0]) circle(chamfer_kerf_width, $fn=3);
    mirror([1,0,0]) translate([-width/2-radius_x, 0]) circle(chamfer_kerf_width, $fn=3);
    
    rotate([0, 0, 90]) translate([-height/2-radius_y, 0]) circle(chamfer_kerf_width, $fn=3);
    mirror([0,1,0]) rotate([0, 0, 90]) translate([-height/2-radius_y, 0]) circle(chamfer_kerf_width, $fn=3);
    
    rotate([0, 0, 45-chamfer_offset_angle]) translate([-height/2-radius_y/2, 0]) circle(chamfer_kerf_width, $fn=3);
rotate([0, 0, -45+chamfer_offset_angle]) translate([-height/2-radius_y/2, 0]) circle(chamfer_kerf_width, $fn=3);
    
rotate([0, 0, 135+chamfer_offset_angle]) translate([-height/2-radius_y/2, 0]) circle(chamfer_kerf_width, $fn=3);
rotate([0, 0, -135-chamfer_offset_angle]) translate([-height/2-radius_y/2, 0]) circle(chamfer_kerf_width, $fn=3);
}

module sheet_kerf() { 
    translate([width/2+radius_x, 0]) square([sheet_kerf_length, sheet_thickness],true); 
    translate([-width/2-radius_x, 0]) square([sheet_kerf_length, sheet_thickness],true); 
    
    translate([0, height/2+radius_y]) square([sheet_thickness, sheet_kerf_length],true); 
    translate([0, -height/2-radius_y]) square([sheet_thickness, sheet_kerf_length],true); 
    
    rotate([0,0,45]) translate([0, height/2+radius_y]) square([sheet_thickness, sheet_kerf_length],true);
rotate([0,0,-45]) translate([0, height/2+radius_y]) square([sheet_thickness, sheet_kerf_length],true);
    
rotate([0,0,135]) translate([0, height/2+radius_y]) square([sheet_thickness, sheet_kerf_length],true);
rotate([0,0,-135]) translate([0, height/2+radius_y]) square([sheet_thickness, sheet_kerf_length],true);
}

module base_shape() {
    hull() {
        translate([width/2, 0]) circle(radius_x);
        translate([-width/2, 0]) circle(radius_x);
        translate([0, height/2]) circle(radius_y);
        translate([0, -height/2]) circle(radius_y);
    }
}