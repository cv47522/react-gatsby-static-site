//  STANDARD
scale([2,2]) {
    hinge(horRepeat=16, vertRepeat=2, patWidth=1, patHeight=22, align=22/2, border=15) 
            standard(20, kerf=0.4);
}

//  DIAMONDS
translate([110, 0]) scale([2,2]) {
    hinge(horRepeat=15, vertRepeat=4, patWidth=1.8, patHeight=11.5, align=11.5/2, border=10) 
        scale([2,10]) 
            diamonds();
}            

//  PARQUET
translate([110,-100]) scale([2,2]) {
    hinge(horRepeat=5, vertRepeat=7, patWidth=5.33, patHeight=6.66, align=6.66/2, vflip=true, border=10) parquet(2,6);
}

//  SNAIL
size = 8; // each square size
kerf = 0.5;
translate([0,-100]) scale([2,2]) {
    hinge(horRepeat=4, vertRepeat=3, patWidth=size, patHeight=2*size, border=10, align=size)
    translate([size/8,0]) snail(a=size,kerf=kerf);
}

/////////////////////////
//  Hinge module
/////////////////////////


module hinge(horRepeat=5, vertRepeat=5, patWidth, patHeight, border, align=0, voffset=0, vflip=false, hflip=false) 
//  Parameters:
//      horRepeat   -   number of horizontal tile repetitions
//      vertRepeat  -   number of vertical tile repetitions
//      patWidth    -   tile width
//      patHeight   -   tile height
//      border      -   additional space at left and right 
//                          of the band
//      align       -   vertical offset added to tile odd columns
//      voffset     -   an overall vertical offset of all tiles
//      vflip       -   boolean: if true, tiles of odd columns are 
//                          vertically flipped
//      hflip       -   boolean: if true, tiles of odd columns are 
//                          horizontally flipped
render(){
    bord = border == undef ? horRepeat*patWidth/4 : border;
    // offset to center the hinge at the origin
    xi = -horRepeat*patWidth/2;
    yi = -vertRepeat*patHeight/2;
    // the hinge bounding box
    hsq = [horRepeat*patWidth, vertRepeat*patHeight];
    // the whole band bounding box
    bsq = hsq + [2*bord, 0];
    echo("Band height = ", hsq[1]);
    echo("Hinge width = ", hsq[0]);
    echo("Whole band width", bsq[0]);
    difference(){
        square(bsq, center=true);
        scal = [ hflip? -1: 1, vflip? -1: 1];
        intersection() {
            // the bounding box of the hinge itself
            square(hsq, center=true);
            for(ix=[-1:horRepeat]) { // for each column
                x = xi + ix*patWidth + patWidth/2; 
                yoffseti = voffset + ((ix%2) ? align : 0) ;
                yoffset  = yoffseti - ceil(yoffseti/patHeight)*patHeight;
                // one extra tile at each end to allow for offsettings
                for(iy=[-1:vertRepeat+1]) { 
                    y = yi + (iy*patHeight + yoffset) + patHeight/2;
                    translate([x,y])
                        if(ix%2==0) scale(scal) children();
                        else children();
                }
            }
        }
    }
}

///////////////
//  Patterns
///////////////

module line(p1,p2,thick) 
    hull(){ 
        translate(p1) circle(thick); 
        translate(p2) circle(thick); 
    }

module standard(size,kerf=0.2, rounded=true){
    rnd = rounded && (kerf>0.5);
    if(rnd)
        line([0,-size/2],[0,size/2],thick=kerf/2);
    else
        square([kerf, size], center=true);
}

module diamonds() {
    rotate(45) square(1/sqrt(2),center=true);
}

module parquet(size, width){
    w = width==undef ? size/2 : width;
    rotate(45) square([w, size*sqrt(2)], center=true);
}

module snail(a=20,b,kerf=0.5) {
    c = b==undef ? a : b/2;
    p = [[-1,2],[-1,4],[1,4],[1,0],[-3,0],
         [-3,-4],[1,-4],[1,-2],[-1,-2]
    ]*[[a,0],[0,c]]/5;
    q = [[0,3],[0,1],[-2,1],[-2,5],[2,5],
         [2,9],[-2,9],[-2,7],[0,7]
    ]*[[a,0],[0,c]]/5;
    for(i=[1:len(p)-1]) line(p[i-1],p[i],kerf);
    for(i=[1:len(q)-1]) line(q[i-1],q[i],kerf);
}