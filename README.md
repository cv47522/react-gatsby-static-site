# Wan-Ting's Project Documentation for Fab Academy

**Static Project Website (built by React.js + [Gatsby](https://www.gatsbyjs.com/) + GitLab CI): <http://fabacademy.wantinghsieh.com>**

## Installation

1. Run the following commands in order:

    ```bash
    cd /path/to/repository/folder
    npm install   # install node modules from package.json
    npm run start  # run the server locally
    ```

2. Type the link in a browser: <http://localhost:8000/>