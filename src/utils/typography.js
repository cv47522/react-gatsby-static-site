import Typography from "typography"
// import kirkhamTheme from "typography-theme-kirkham"
import oceanBeachTheme from "typography-theme-ocean-beach"

const typography = new Typography(oceanBeachTheme)

export default typography
export const rhythm = typography.rhythm