import React from 'react';
import Layout from '../components/layout';
import WeekBanner from '../components/WeekBanner';
import {
  Container, Row, Col, Table,
  ListGroup, ListGroupItem,
  ListGroupItemHeading, ListGroupItemText }
from 'reactstrap';

export default function WeekThree() {
  return (
    <Layout>
      <WeekBanner weekId='3' />
      <div className='mb-5'>
        <Container>
          <Row>
            <Col className='h3'>Individual Vinyl Cutting Project</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Materials</ListGroupItemHeading>
                    <Table striped>
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Name</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <th scope="row">1</th>
                         <td>Vinyl Cutter: Roland GX-24 (<a href='https://www.rolanddga.com/support/products/cutting/camm-1-gx-24-24-vinyl-cutter'>Manual</a>)</td>
                       </tr>
                       <tr>
                         <th scope="row">2</th>
                         <td>Heat Press Machine</td>
                       </tr>
                       <tr>
                         <th scope="row">3</th>
                         <td>T-Shirt Heat Transfer Stickers</td>
                       </tr>
                     </tbody>
                   </Table>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Setup: Vector Image Adjustment in Illustrator</ListGroupItemHeading>
                    <p>I used my vector painting image as the material for vinyl cutting.</p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-setup-1.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-setup-2.png' /></Col>
                    </Row>
                    <p>
                      Before cutting, I did some adjustment so that the path of the blade could work as my expectation.
                      In order to simplify the vectors of my image, I chose which layer is going to be trimmed with another one.
                      I did this by clicking the "Trim" option of the "Pathfinders" dialogue box in Illustrator.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-setup-3.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-setup-5.png' /></Col>
                    </Row>
                    <p>After this, I refilled the color of different parts and grouped parts with the same color.</p>
                    <Row>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-setup-4.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-setup-6.png' /></Col>
                    </Row>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Fabrication: Roland GX-24 Vinyl Cutter</ListGroupItemHeading>
                    <p>The finished file was then sent to the machine wainting for cutting.</p>
                    <p>First, I measured the width of the inserted material sheet by selecting the "EDGE" mode.</p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-cut-0.jpg' /></Col>
                      <Col lg='3'><img alt='' src='../../03-WeekThree/vinylcutter-cut-1.jpg' /></Col>
                      <Col lg='3'><img alt='' src='../../03-WeekThree/vinylcutter-cut-2.jpg' /></Col>
                    </Row>
                    <p>Then, since I was going to cut the same image 6 times with 6 colors, I chose "Output Selected Lines" option,
                      selected the first mirrored layer and clicked "Output the Paths".</p>
                    <Row className='mb-3'>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-cut-3.jpg' /></Col>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-cut-4.jpg' /></Col>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-cut-5.jpg' /></Col>
                    </Row>
                    <p>After the cutting was done, I pressed "MENU" then"ENTER" button to exit the cutting mode. </p>
                    <Row className='mb-3'>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-cut-6.jpg' /></Col>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-cut-7.jpg' /></Col>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-cut-8.jpg' /></Col>
                    </Row>
                    <p>I kept the parts I would like to transfeer to a T-shirt.</p>
                    <Row className='mb-3'>
                      <Col lg='3'><img alt='' src='../../03-WeekThree/vinylcutter-layer-0.jpg' /></Col>
                      <Col lg='3'><img alt='' src='../../03-WeekThree/vinylcutter-layer-1.jpg' /></Col>
                      <Col lg='3'><img alt='' src='../../03-WeekThree/vinylcutter-layer-2.jpg' /></Col>
                      <Col lg='3'><img alt='' src='../../03-WeekThree/vinylcutter-layer-3.jpg' /></Col>
                    </Row>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>T-Shirt Heat Transfer</ListGroupItemHeading>
                    <p>
                      Finally, we used a heat press machine for transfering the cutting image to a T-shirt.
                      The machine should be heated up to 165℃ to make sure it can transfer the pattern successfully.
                      Since there are total 6 layers I would like to transfer to the T-shirt,
                      I pressed each layer (except the last yellow layer) and waited for 3 seconds then changed to the next one (normally 12 seconds for total 1 layer).
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-heat-1.jpg' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-heat-8.jpg' /></Col>
                    </Row>
                    <Row className='mb-3'>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-heat-2.jpg' /></Col>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-heat-3.jpg' /></Col>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-heat-4.jpg' /></Col>
                    </Row>
                    <Row className='mb-3'>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-heat-5.jpg' /></Col>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-heat-6.jpg' /></Col>
                      <Col lg='4'><img alt='' src='../../03-WeekThree/vinylcutter-heat-9.jpg' /></Col>
                    </Row>
                    <p>
                      I was lucky to find a reflecting sheet in a trash box, which results in a cool T-shirt with a fancy cat.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-heat-7.jpg' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/vinylcutter-heat-10.jpg' /></Col>
                    </Row>
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
        <hr/>
        <Container className='pb-5'>
          <Row>
            <Col className='h3'>Group Assignment: Characterize Our Laser Cutter</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Recorded by <a href='https://bluet87.gitlab.io/fab-academy/assignments/week-03/'>Jasmine</a></ListGroupItemHeading>
                </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Materials</ListGroupItemHeading>
                      <Table striped responsive>
                       <thead>
                         <tr>
                           <th>#</th>
                           <th>Name</th>
                           <th>Type</th>
                           <th>(Cutting) Area</th>
                           <th>Height</th>
                           <th>Other</th>
                         </tr>
                       </thead>
                       <tbody>
                         <tr>
                           <th scope="row">1</th>
                           <td>Laser Cutter: EPILOG Legend 36EXT (<a href='https://www.epiloglaser.com/downloads/pdf/ext_4.22.10.pdf'>Manual</a>)</td>
                           <td>60W CO<sub>2</sub></td>
                           <td>914 x 610mm</td>
                           <td>305mm</td>
                           <td><ul>
                             <li>kerf: 0.125mm</li>
                             <li>supporting file formats: .ai, .cdr, .pdf, and .svg</li>
                             <li><a href='https://wiki.aalto.fi/display/AF/Laser+Cutter+Epilog+Legend+36EXT'>Fablab Step-by-Step Guideline</a></li>
                           </ul></td>
                         </tr>
                         <tr>
                           <th scope="row">2</th>
                           <td>Laser Cutting Sheets</td>
                           <td>MDF</td>
                           <td>60 x 60mm</td>
                           <td>4mm</td>
                           <td></td>
                         </tr>
                       </tbody>
                     </Table>
                  </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
        <hr/>
        <Container className='pb-5'>
          <Row>
            <Col className='h3'>Individual Laser Cutting Project 1: Parametric Press-Fit Kit</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Material Sheets + Cutting Parameters</ListGroupItemHeading>
                    <Table striped responsive>
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Area</th>
                         <th>Thickness</th>
                         <th>Speed</th>
                         <th>Power</th>
                         <th>Frequency</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <th scope="row">1</th>
                         <td>MDF</td>
                         <td>300 x 200mm</td>
                         <td>6mm</td>
                         <td>8%</td>
                         <td>75%</td>
                         <td>500Hz</td>
                       </tr>
                     </tbody>
                   </Table>
                 </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Design Tool: OpenSCAD</ListGroupItemHeading>
                    <p>
                      I defined 3 modules (<code>base_shape()</code>, <code>sheet_kerf()</code> and <code>chamfer_kerf()</code>) and differenced them to form the final combination.
                      I can transform some features by simply changing the values of <code>laser_kerf_width</code>, <code>sheet_thickness</code>, <code>sheet_kerf_length</code>, etc.
                    </p>
                    <a href='../../03-WeekThree/lasercut-openscad-1.png'>
                      <img alt='' className='mb-3' src='../../03-WeekThree/lasercut-openscad-1.png' />
                    </a>
                    <p>
                      It is convenient to use a <code>for</code> loop function in OpenSCAD to generate multiple units at once.
                    </p>
                    <img alt='' className='mb-3' src='../../03-WeekThree/lasercut-openscad-2.png' />
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Cutting Tool: Illustrator</ListGroupItemHeading>
                    <p>
                      It is important to make sure that the <code>stroke width</code> of all the cutting vectors is set to <code>0.001 with #000000 black color</code>.
                    </p>
                    <img alt='' className='mb-3' src='../../03-WeekThree/lasercut-setup-1.png' />
                    <p>Then I opened the "Print" dialogue box and did the following settings step by step.</p>
                    <a href='../../03-WeekThree/lasercut-setup-2.png'><img alt='' className='mb-3' src='../../03-WeekThree/lasercut-setup-2.png' /></a>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/lasercut-setup-3.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/lasercut-setup-5.png' /></Col>
                    </Row>
                    <p>
                      Here is <a href='https://wiki.aalto.fi/display/AF/Laser+Cutter+Settings'>a table for the setting of Fablab's laser cutter</a> according to the thickness and the material of a cutting sheet.
                    </p>
                    <img alt='' className='mb-3' src='../../03-WeekThree/lasercut-setup-4.png' />

                    <p>Below is the documentation of the laser cutting process:</p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' className='mb-3' src='../../03-WeekThree/lasercut-cutting-1.jpg' /></Col>
                      <Col lg='6' className='mb-3'><video width='100%' controls>
                        <source src="../../03-WeekThree/lasercut-cutting-0-small.mp4" type="video/mp4" />
                      </video></Col>
                    </Row>
                  </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>End Result</ListGroupItemHeading>
                  <Row className='mb-3'>
                    <Col lg='4'><img alt='' src='../../03-WeekThree/lasercut-final-1.jpg' /></Col>
                    <Col lg='4'><img alt='' src='../../03-WeekThree/lasercut-final-2.jpg' /></Col>
                    <Col lg='4'><img alt='' src='../../03-WeekThree/lasercut-final-3.jpg' /></Col>
                  </Row>
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
        <hr/>
        <Container>
          <Row>
            <Col className='h3'>Individual Laser Cutting Project 2: Living Hindge</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Material Sheets + Cutting Parameters</ListGroupItemHeading>
                    <Table striped responsive>
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Name</th>
                         <th>Area</th>
                         <th>Thickness</th>
                         <th>Speed</th>
                         <th>Power</th>
                         <th>Frequency</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <th scope="row">1</th>
                         <td>Paper</td>
                         <td>150 x 150mm</td>
                         <td>0.2mm</td>
                         <td>90%</td>
                         <td>55%</td>
                         <td>500Hz</td>
                       </tr>
                     </tbody>
                   </Table>
                 </ListGroupItem>
                 <ListGroupItem action>
                   <ListGroupItemHeading>Design Tool: OpenSCAD</ListGroupItemHeading>
                     <p>
                       Since it is normal to use linear pattern to build a living hinge, I was intereseted in exploring more possibilities to construct other kinds of living hinges.
                       I found some discussion about how to build a living hinge from the <a href='http://forum.openscad.org/Living-Hinges-td18217.html'>OpenSCAD forum</a>.
                       I then used 4 modules as a reference and experimented them by transforming them and changing their parameters.
                     </p>
                     <a href='../../03-WeekThree/lasercut-openscad-3.png'>
                       <img alt='' className='mb-3' src='../../03-WeekThree/lasercut-openscad-3.png' />
                     </a>
                 </ListGroupItem>
                 <ListGroupItem action>
                   <ListGroupItemHeading>End Result</ListGroupItemHeading>
                     <p>
                       The sheet with different patterns has distinct flexibility.
                     </p>
                     <Row className='mb-3'>
                       <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/lasercut-living-hinge-test-1.gif' /></Col>
                       <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/lasercut-living-hinge-test-2.gif' /></Col>
                     </Row>
                     <Row className='mb-3'>
                       <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/lasercut-living-hinge-test-3.gif' /></Col>
                       <Col lg='6' className='mb-3'><img alt='' src='../../03-WeekThree/lasercut-living-hinge-test-4.gif' /></Col>
                     </Row>
                     <p>Below is the documentation of the laser cutting process:</p>
                     <Row className='mb-3'>
                       <Col lg='6' className='mb-3'><img alt='' className='mb-3' src='../../03-WeekThree/lasercut-living-hinge-1.jpg' /></Col>
                       <Col lg='6' className='mb-3'><video width='100%' controls>
                         <source src="../../03-WeekThree/lasercut-living-hinge-snail-small.mp4" type="video/mp4" />
                       </video></Col>
                     </Row>
                 </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
      </div>
    </Layout>
  );
}

