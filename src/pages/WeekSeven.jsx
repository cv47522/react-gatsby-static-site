import React from 'react';
import Layout from '../components/layout';
import WeekBanner from '../components/WeekBanner';
import {
  Container, Row, Col, Table,
  ListGroup, ListGroupItem,
  ListGroupItemHeading, ListGroupItemText }
from 'reactstrap';


export default function WeekSeven() {
  return (
    <Layout>
      <WeekBanner weekId='7' />
      <div>
        <Container className='pb-5'>
          <Row>
            <Col className='h3'>Group Assignment: Characterize Our CNC Machine</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Materials</ListGroupItemHeading>
                      <Table striped responsive>
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>(Cutting) Area</th>
                            <th>Runout</th>
                            <th>Other</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>CNC Milling Machine: Recontech 1312 (<a href='https://www.cnc.fi/recontech-1312.html'>Manual</a>)</td>
                            <td>60W CO<sub>2</sub></td>
                            <td>914 x 610mm</td>
                            <td>?</td>
                            <td><ul>
                              <li>kerf: 0.125mm</li>
                              <li>supporting file formats: .ai, .cdr, .pdf, and .svg</li>
                              <li><a href='https://wiki.aalto.fi/display/AF/Laser+Cutter+Epilog+Legend+36EXT'>Fablab Step-by-Step Guideline</a></li>
                            </ul></td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>Milling Sheets</td>
                            <td>15mm PlyWood</td>
                            <td>600 x 1000mm *2</td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </Table>
                  </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
        <hr/>
        <Container className='pb-5'>
          <Row className='mb-3'>
            <Col className='h3'>Individual CNC Milling Project: Simple Chair & Table</Col>
          </Row>
          <Row className='mb-3'>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Materials</ListGroupItemHeading>
                    <Table striped>
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>CNC Milling Machine: Recontech 1312 (<a href='https://www.cnc.fi/recontech-1312.html'>Manual</a>)</td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>6mm Two-Flute Square Flat Milling Bit</td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>15mm Plywood</td>
                        </tr>
                        <tr>
                          <th scope="row">4</th>
                          <td>12mm Fiberboard</td>
                        </tr>
                      </tbody>
                    </Table>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Design Tool: Fusion 360</ListGroupItemHeading>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Setup: Generate Bit CAM-Toolpath in Manufacture Section of Fusion 360</ListGroupItemHeading>
                    <p>
                      I used <a href='http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers'>hello.serial-UPDI.FT230X</a> and <a href='http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers'>hello.USB-UPDI.FT230X</a> file for my PCB milling fabrication.
                      For milling the traces, we are going to use <code>0.4mm</code> diameter bit for the former one and <code>0.3mm</code> for the latter one to remove some of the top copper and then form the final circuit.
                      The bit with <code>0.8mm</code> diameter is used to cut off the interior (outline) of both boards.
                      Each bit with different diameter has its own suitable cutting speed:
                    </p>
                    <ul>
                      <li>0.3mm bit: tiny traces</li>
                      <li>0.4mm bit: normal traces</li>
                      <li>0.8mm bit: interior (outline)</li>
                    </ul>
                    <Row className='my-3'>
                      <Col lg='4'><img alt='' src='../../07-WeekSeven/pcb-cutting-speed.jpg' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/bits.jpg' /></Col>
                    </Row>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><h6><a href='http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers'>hello.serial-UPDI.FT230X</a>: 0.4mm(traces), 0.8mm(interior)</h6><img alt='' src='../../07-WeekSeven/0.4+0.8mm.jpg' /></Col>
                      <Col lg='6' className='mb-3'><h6><a href='http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers'>hello.USB-UPDI.FT230X</a>: 0.3mm(traces), 0.8mm(interior)</h6><img alt='' src='../../07-WeekSeven/0.3+0.8mm.jpg' /></Col>
                    </Row>

                    <h5>mods Toolpath Setting Comparison</h5>
                    <Table bordered responsive>
                      <thead>
                        <tr>
                          <th>tool diameter</th>
                          <th>cut depth</th>
                          <th>max depth</th>
                          <th>offset number</th>
                          <th>offset stepover</th>
                          <th>direction</th>
                          <th>speed</th>
                          <th>origin: (x, y, z)</th>
                          <th>file</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">0.3mm</th>
                          <td>0.1mm</td>
                          <td>0.1mm</td>
                          <td>4</td>
                          <td>0.5</td>
                          <td>climb</td>
                          <td>1.0mm/s</td>
                          <td>(0, 0, 0)</td>
                          <td><a href='http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.traces.png'>USB-FT230XS-UPDI.traces.png</a></td>
                        </tr>
                        <tr>
                          <th scope="row">0.4mm</th>
                          <td>0.1mm</td>
                          <td>0.1mm</td>
                          <td>4</td>
                          <td>0.5</td>
                          <td>climb</td>
                          <td>1.5mm/s</td>
                          <td>(0, 0, 0)</td>
                          <td><a href='http://academy.cba.mit.edu/classes/embedded_programming/UPDI/FTDI-UPDI.traces.png'>FTDI-UPDI.traces.png</a></td>
                        </tr>
                        <tr>
                          <th scope="row">0.8mm</th>
                          <td>0.6mm</td>
                          <td>1.8mm</td>
                          <td>1</td>
                          <td>0.5</td>
                          <td>climb</td>
                          <td>2.0mm/s</td>
                          <td>(0, 0, 0)</td>
                          <td>
                            <ul className='pl-3'>
                              <li><a href='http://academy.cba.mit.edu/classes/embedded_programming/UPDI/FTDI-UPDI.interior.png'>FTDI-UPDI.interior.png</a></li>
                              <li><a href='http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.interior.png'>USB-FT230XS-UPDI.interior.png</a></li>
                          </ul>
                          </td>
                        </tr>

                      </tbody>
                    </Table>
                    <a href='../../07-WeekSeven/mods_0.8_setting.png'><img alt='' src='../../07-WeekSeven/mods_0.8_setting.png' /></a>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Fabrication: Recontech 1312 CNC Milling Machine</ListGroupItemHeading>
                    <h5 className='mt-5'>Setup: Mach 3 User Interface</h5>
                    <p>
                      After generating the trace file, I then uploaded the interior image to mods with the same steps but different parameters to save the outline cutting toolpath file.
                    </p>

                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/step6.jpg' /></Col>
                    </Row>
                    <h5 className='mt-5'>Use Fixings: Vacuum Table + MDF Sheet</h5>
                    <p>
                      After generating the trace file, I then uploaded the interior image to mods with the same steps but different parameters to save the outline cutting toolpath file.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/SRM_20_z_reachLowestPoint_step1.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/SRM_20_z_setZero_step2.png' /></Col>
                    </Row>
                    <p>Then I lifted up the z-axis for <code>2mm</code> and set it to zero again as well as both x-axis and y-axis.</p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/SRM_20_z_liftUp2mm_step3.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/SRM_20_z_setZeroAgain_step4.png' /></Col>
                    </Row>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/SRM_20_xy_setZero_step5.png' /></Col>
                    </Row>
                    <p>I turned to the milling machince and released the screw to make the bit lower enough to touch the top of the board.</p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/step6.jpg' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/step7_lowerBit.jpg' /></Col>
                    </Row>
                    <p>
                      Before starting cutting, I lifted up the z-axis again for 2mm, slowed down the initial cutting and spindle speed and then pressed <code>Cut</code> button.
                      In order to make the job queue clean, I first <code>Delete All</code> jobs and then <code>Add</code> the new one. After that, I clicked <code>Output</code> to cut the traces or the outline.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/SRM_20_slowDownSpeed_step9.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/SRM_20_loadCuttingFile_step10.png' /></Col>
                    </Row>
                    <p>If there is not any problem with the initial cutting, I then adjusted the values of both speeds to normal.</p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/SRM_20_speedUp_step11.png' /></Col>
                      <Col lg='6' className='mb-3'><video width='100%' controls>
                        <source src="../../07-WeekSeven/USB-FT230XS-UPDI.traces-small.mp4" type="video/mp4" />
                      </video></Col>
                    </Row>

                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><h6>hello.serial-UPDI.FT230X</h6><img alt='' src='../../07-WeekSeven/hello.serial-UPDI.FT230X-1.jpg' /></Col>
                      <Col lg='6' className='mb-3'><h6>hello.USB-UPDI.FT230X</h6><img alt='' src='../../07-WeekSeven/hello.USB-UPDI.FT230X-1.jpg' /></Col>
                    </Row>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Assembling & Sanding</ListGroupItemHeading>
                  <h6 className='mt-5'>Electronic Components</h6>
                  <Table bordered responsive>
                    <thead>
                      <tr>
                        <th>board</th>
                        <th>SMD IC</th>
                        <th>SMD resistor</th>
                        <th>SMD capacitor</th>
                        <th>SMD header</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><a href='http://academy.cba.mit.edu/classes/embedded_programming/UPDI/FTDI-UPDI.png'>hello.serial-UPDI.FT230X</a></th>
                        <td>-</td>
                        <td>4.99k Ω *1</td>
                        <td>-</td>
                        <td>
                          <ul className='pl-3'>
                          <li>2.54mm male 1 row horizon header *1</li>
                          <li>2W 2.54mm PTH SOCKET SIL SMT *1</li>
                        </ul>
                      </td>
                      </tr>
                      <tr>
                        <th scope="row"><a href='http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.png'>hello.USB-UPDI.FT230X</a></th>
                        <td>FT230XS</td>
                        <td>
                          <ul className='pl-3'>
                            <li>4.99k Ω *1</li>
                            <li>49 Ω *2</li>
                          </ul>
                        </td>
                        <td>
                          <ul className='pl-3'>
                            <li>1 uF *1</li>
                            <li>10 pF *2</li>
                          </ul>
                        </td>
                        <td>2W 2.54mm PTH SOCKET SIL SMT *1</td>
                      </tr>
                    </tbody>
                  </Table>

                  <p>
                    It is harder to solder SMD components than the DIP ones. Therefore, I used a magnifier to make sure that I can clearly see each pin of the components while soldering the bords especially the SMD IC.
                  </p>
                  <Row className='mb-3'>
                    <Col lg='4'><img alt='' src='../../07-WeekSeven/magnifier.jpg' /></Col>
                  </Row>

                  <h6 className='mt-5'>hello.serial-UPDI.FT230X</h6>
                  <Row className='mb-3'>
                    <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/hello.serial-UPDI.FT230X-2.jpg' /></Col>
                    <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/hello.serial-UPDI.FT230X-3.jpg' /></Col>
                  </Row>
                  <h6 className='mt-5'>hello.USB-UPDI.FT230X</h6>
                  <Row className='mb-3'>
                    <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/hello.USB-UPDI.FT230X-2.jpg' /></Col>
                    <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/hello.USB-UPDI.FT230X-3.jpg' /></Col>
                  </Row>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Problem & Solution</ListGroupItemHeading>
                  <p>
                    I spent a lot of time on adjusting the origin of the z-axis of the SRM-20 milling machine since it didn't actually cut through the copper layer but lifted up after pessing the <code>Output</code> button at the beginning.
                    Then I found out that the problem is that the origin value of the <code>Roland SRM-20 milling machine</code> module which I didn't change but left it as default values (10, 10, 10) on the <a href='http://mods.cba.mit.edu/'>mods</a> website had made the machine lift up for 10mm during the cutting process.
                  </p>
                  <Row className='mb-3'>
                    <Col lg='4'><img alt='' src='../../07-WeekSeven/problem-1.png' /></Col>
                  </Row>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>End Result</ListGroupItemHeading>
                  <p>
                    I programmed the <a href='https://gitlab.com/aaltofablab/hello-attiny1614-blink'>Hello ATtiny1614 Blink board</a> with the hello.USB-UPDI.FT230X I made by uploading the Arduino script through the command lines in the terminal.
                    Finally, my board works!
                  </p>
                  <Row className='mb-3'>
                    <Col lg='6' className='mb-3'><img alt='' src='../../07-WeekSeven/hello.USB-UPDI.FT230X-functional.jpg' /></Col>
                    <Col lg='6' className='mb-3'><video width='100%' controls>
                      <source src="../../07-WeekSeven/hello.USB-UPDI.FT230X-functional-small.mp4" type="video/mp4" />
                    </video></Col>
                  </Row>
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
      </div>
    </Layout>
  );
}

