import React from 'react';
import Layout from '../components/layout';
import WeekBanner from '../components/WeekBanner';
import {
  Container, Row, Col,
  ListGroup, ListGroupItem,
  ListGroupItemHeading, ListGroupItemText }
from 'reactstrap';

import {
  dependencies,
  devDpendencies,
  scripts,
  webpackConfig,
  gitlabCICD
} from '../components/WeekOneCode.jsx';

export default function WeekOne() {
  return (
    <Layout>
      <WeekBanner weekId='1' />
      <div className='mb-5'>
        <Container>
          <Row>
            <Col className='h3'>Read the <a href='http://fab.cba.mit.edu/about/charter/'>Fab Charter</a></Col>
          </Row>
          <Row>
            <Col>
              <p>
                To me, the documentations and online resources that the fab academy community
                has provided are really useful.
                It offers various ways of thinking and making almost everthing by using open source tools such as mods and OpenSCAD.
              </p>
            </Col>
          </Row>
        </Container>
        <hr />
        <Container className='pb-5'>
          <Row className='my-5'>
            <Col className='lead'>
              As Git is a distributed version-control system, it could be used as a server out of the box.
              It's shipped with built-in command git daemon which starts simple TCP server running
              on the GIT protocol. Dedicated Git HTTP servers help (amongst other features)
              by adding access control, displaying the contents of a Git repository via the web interfaces,
              and managing multiple repositories. Already existing Git repositories can be cloned
              and shared to be used by others as a centralized repo.
              It can also be accessed via remote shell just by having the Git software installed
              and allowing a user to log in. Git servers typically listen on TCP port 9418.
            </Col>
          </Row>
          <Row>
            <Col className='h3'>Git</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Setup</ListGroupItemHeading>
                    <p><kbd>git init</kbd> : initialize a repository</p>
                    <p><kbd>git config --global user.name "Firstname Lastname"</kbd> : set name for commits</p>
                    <p><kbd>git config --global user.email "address@site"</kbd> : set email for commits</p>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Add & Commit</ListGroupItemHeading>
                    <p><kbd>git remote add origin [SERVER_URL]</kbd> : push to a remot repository (ex. GitHub or GitLab)</p>
                    <p><kbd>git add .</kbd> : stage all changed files to commit</p>
                    <p><kbd>git commit -m "commit message"</kbd> : commit changes</p>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Update & Merge</ListGroupItemHeading>
                    <p><kbd>git push</kbd> : push changes</p>
                    <p><kbd>git merge [BRANCH_NAME]</kbd> : merge branch</p>
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
        <hr/>
        <Container className='pb-5'>
          <Row className='my-5'>
            <Col className='lead'>
              This project website is built based on React.js framework
              as its front-end development and Node.js as its back-end.
            </Col>
          </Row>
          <Row>
            <Col className='h3'>Why React.js?</Col>
          </Row>
          <Row className='mb-5'>
            <Col>
              React.js is a component based programming language,
              which means that I can reuse the same component such as
              the "Jumbotron" on the top of this page and replace it
              with another topic title, description, image and date in any other pages
              I want to show by just calling its component's tag name <code>&#60;WeekLayout /&#62;</code>.
            </Col>
          </Row>
          <Row>
            <Col className='h3'>Does Node.js backend work on GitLab CI/CD?</Col>
          </Row>
          <Row className='mb-5'>
            <Col>
              <p>
                By setting <code>.gitlab-ci.yaml</code> properly,
                Node.js backend environment can be installed during the running pipleline.
                Since the documentation website doesn't need to receive any dynamic data, I made it remain static.
              </p>
              <p>
                There is also an open source package called <a href='https://gitlab.com/gitlab-org/gitlab-runner'>GitLab Runner</a> which can be used to cooperate with GitLab to run:
              </p>
              <ul>
                <li>Multiple jobs concurrently.</li>
                <li>Use multiple tokens with multiple server (even per-project).</li>
                <li>Limit number of concurrent jobs per-token.</li>
              </ul>
              <p>Here is the explanation about how my static documentation website works during the GitLab pipeline:</p>
              <pre>
                {gitlabCICD}
              </pre>
            </Col>
          </Row>
          <Row>
            <Col className='h3'>Web Development: React.js + React Router + Webpack</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Setup: Node.js</ListGroupItemHeading>
                    <p>install
                      <a href='https://nodejs.org/en/download/'>
                       &nbsp;Node.js</a>
                    </p>
                    <p>create folders and js/jsx files as the same structure of the
                      <a href='https://gitlab.com/cv47522/fab-academy'>
                      &nbsp;project on GitLab</a>
                    </p>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Install Package Dependencies</ListGroupItemHeading>
                    <p><kbd>npm i --save [PACKAGE_NAME]</kbd> : save below packages</p>
                    <Col>
                      <pre>
                        {dependencies}
                      </pre>
                    </Col>
                    <p><kbd>npm i --save-dev [PACKAGE_NAME]</kbd> : save below packages for development purpose</p>
                    <Col>
                      <pre>
                        {devDpendencies}
                      </pre>
                    </Col>
                  </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Add Scripts to package.json for Webpack Bundle</ListGroupItemHeading>
                  <Col>
                    <pre>
                      {scripts}
                    </pre>
                  </Col>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Create & Edit webpack.config.js</ListGroupItemHeading>
                  <Col>
                    <pre>
                      {webpackConfig}
                    </pre>
                  </Col>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Build bundle.js or Start Local Server</ListGroupItemHeading>
                  <Col>
                    <p><kbd>npm run build</kbd> : build index.bundle.js and vendor.bundle.js in dist folder</p>
                    <p><kbd>npm run start</kbd> : start and listen to webpack server on port 6060</p>
                  </Col>
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
        <hr/>
        <Container>
          <Row className='my-5'>
            <Col className='lead'>
              Image optimization is about reducing the file size of the images displayed on a website as much as possible
              without sacrificing quality so that the load times of a web page remain low.
              It’s also about image SEO. That is, getting project images and decorative images
              to rank on Google and other image search engines.
            </Col>
          </Row>
          <Row>
            <Col className='h3'>Image Optimization</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Tool 1: Photoshop</ListGroupItemHeading>
                  <p>
                    Since I am quite familiar with using Photoshop to process images for printing or design purpose.
                    So I choose it for my initial image optimization.
                  </p>
                  <p className='mt-4'>It is intuitive to adjust the scale of an image
                    by inputing the width, height or resolution you want to the "image size" dialogue box in Photoshop.
                  </p>
                  <img alt='' src='../../01-WeekOne/photoshop.png' />
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Tool 2: TinyPNG</ListGroupItemHeading>
                  <p>
                    After adjusting the width, height and resolution of my images in Photoshop,
                    I then upload the images to a online image compression tool <a href='https://tinypng.com/'>TinyPNG</a> which uses smart lossy compression techniques
                    to reduce the imge size without sacrificing quality of the images.
                  </p>
                  <img alt='' src='../../01-WeekOne/TinyPNG.png' />
                  <p className='mt-4'>Here is the reducing percentage of the images used for this week's documentation:</p>
                  <img alt='' src='../../01-WeekOne/TinyPNG-reduce.png' />
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Check Tool: Google PageSpeed Insights</ListGroupItemHeading>
                    <p>
                      <a href='https://developers.google.com/speed/pagespeed/insights/?hl=en'>
                      Google PageSpeed Insights
                    </a> is a handy tool to check which parts of your website slow down the performance
                      and give some advice about how to fix it including image compression.
                    </p>
                    <img alt='' src='../../01-WeekOne/PageSpeed-1.png' />
                    <p className='mt-4'>Here is the testing result of my fab academy website:</p>
                    <img alt='' src='../../01-WeekOne/PageSpeed-2.png' />
                    <img alt='' src='../../01-WeekOne/PageSpeed-3.png' />
                    <img alt='' src='../../01-WeekOne/PageSpeed-4.png' />
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
      </div>
    </Layout>
  );
}

