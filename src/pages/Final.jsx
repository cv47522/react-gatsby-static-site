import React from 'react';
import Layout from '../components/layout';
import WeekBanner from '../components/WeekBanner';

import {
  Container, Row, Col,
  ListGroup, ListGroupItem,
  ListGroupItemHeading, ListGroupItemText }
from 'reactstrap';

export default function Final() {
  return (
    <Layout>
      <WeekBanner weekId='18' />
      <div>
        <Container className='mb-5'>
          <Row className='mt-5'>
            <Col className='lead'>
              “Breath of Flora” is an interactive sound and light mechanical flower.
              Infrared sensors and sound detection sensors are used in the artwork
              as the main technique to interact with people.
              The work adopts “Listening” as the concept and is going to be displayed
              in the oldest and longest functioning mental hospital in Finland
               —— Lapinlahti Hospital around April 2020.
             </Col>
          </Row>
          <Row className='mt-5'>
            <Col className='h3'>Sketch & Movement Reference</Col>
          </Row>
          <Row>
            <Col sm='6' xs='12'>
              <a href='../../Final/flower-sketch.jpg'>
                <img alt=''  width='100%' src='../../Final/flower-sketch.jpg' alt='sketch'/>
              </a>
            </Col>
            <Col sm='6' xs='12'>
              <img alt=''  width='100%' src='../../Final/flower-movement.gif' alt='movement'/>
            </Col>
          </Row>
          <Row className='mt-5'>
            <Col className='h3'>Background</Col>
          </Row>
          <Row>
            <Col>Since Lapinlahti mental hospital was rather advanced for its time,
              surrounded by the sea and large gardens to calm and cure patients.
              From the beginning, gardening was seen as a key part of therapy
              and nature as a soothing place to relieve stress.
              Today Lapinlahti hospital is a communal living room open for everyone.
              It functions as an oasis of culture, art and events and as
              a haven of wellbeing for the mind and body.
            </Col>
          </Row>
          <Row className='mt-3 mb-5'>
            <Col sm='6' xs='12'>
              <a href='../../Final/hospital-1.jpg'>
                <img alt='' width='100%' src='../../Final/hospital-1.jpg' alt='hospital-1'/>
              </a>
            </Col>
            <Col sm='6' xs='12'>
              <a href='../../Final/hospital-2.jpg'>
                <img alt='' width='100%' src='../../Final/hospital-2.jpg' alt='hospital-2'/>
              </a>
            </Col>
          </Row>
          <Row className='mt-5'>
            <Col className='h3'>Features</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Art + Technology</ListGroupItemHeading>
                  <ListGroupItemText>
                    By combining art with technology, it actively educates audiences to
                    put themselves in patients’ shoes and introspect whether
                    if they have the right attitude to hearken to sick people,
                    which enhances the connection between new media art and psychiatric hospitals.
                  </ListGroupItemText>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Dynamic v.s. Static</ListGroupItemHeading>
                  <ListGroupItemText>
                    Compared to still sculptures decorated in the majority of public space,
                    the kinetic installation is made from electronic components and programmed with codes
                    for the sake of returning sound and light feedback depending on the length
                    and noise level during the interaction.
                  </ListGroupItemText>
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Participatory Art</ListGroupItemHeading>
                  <ListGroupItemText>
                    Since the way of how every passerby interacts with the artwork is unique,
                    the installation has also become a kind of participatory art which engages public participation
                    in the innovative process, letting them become co-authors, editors, and observers of the work
                    and is deficient without audiences' physical interaction, making it into an exclusive artwork ultimately.
                  </ListGroupItemText>
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
          <Row className='mt-5'>
            <Col className='h3'>Inspiration</Col>
          </Row>
          <Row>
            <Col>
              <div className="embed-responsive embed-responsive-16by9">
                <iframe className="embed-responsive-item" title='Korean Artist'
                  src="https://www.youtube.com/embed/mwue6cvG6aY" allowFullScreen></iframe>
              </div>
             </Col>
           </Row>
           <Row className='mt-3'>
             <Col>
               <div className="embed-responsive embed-responsive-16by9">
                 <iframe className="embed-responsive-item" title='Mechanical tulip mechanism'
                   src="https://www.youtube.com/embed//_hzQIuPzaBM" allowFullScreen></iframe>
               </div>
              </Col>
            </Row>
        </Container>
      </div>
     </Layout>
  );
}
