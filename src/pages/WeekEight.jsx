import React from 'react';
import { Link } from "gatsby";
import Layout from '../components/layout';
import WeekBanner from '../components/WeekBanner';

import {
	attiny412_blink,
	attiny412_blink_simplified,
	attiny412_echo,
  } from '../components/WeekEightCode.jsx';

import {
	Container,
	Row,
	Col,
	Table,
	ListGroup,
	ListGroupItem,
	ListGroupItemHeading
} from 'reactstrap';

export default function WeekEight({ location }) {
	return (
	  <Layout>
		{/* /WeekEight/ */}
		{/* <h1>The location is {location.pathname}</h1> */}
		<WeekBanner weekId='8'/>

		<div className="mb-5">
			<Container className="pb-5">
				<Row>
					<Col className="h3">Individual Assignment: ATtiny 412 Double-Sided Board Programming</Col>
				</Row>
				<Row>
					<Col>
						<ListGroup>
							<ListGroupItem action>
								<ListGroupItemHeading>Materials</ListGroupItemHeading>
								<Table striped responsive id="material-table">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>
											<th>Source</th>
											<th>Description</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">1</th>
											<td>ATtiny 412 Double-Sided Board</td>
											<td>
												<Link to="/WeekSix">Week 6: Electronics Design</Link>
											</td>
											<td>The main board for programming.</td>
										</tr>
										<tr>
											<th scope="row">2</th>
											<td>USB FTDI FT230XS Host Board</td>
											<td>
												<a href="https://gitlab.com/aaltofablab/usb-ftdi-ft230xs">
													Aalto Fablab
												</a>
											</td>
											<td>
												Used for replacing a{' '}
												<a href="https://www.ftdichip.com/Products/Cables/USBTTLSerial.htm">
													FTDI cable
												</a>.
											</td>
										</tr>
										<tr>
											<th scope="row">3</th>
											<td>USB UPDI FT230XS Programmer</td>
											<td>
												<a href="https://gitlab.com/aaltofablab/usb-updi-ft230xs">
													Aalto Fablab
												</a>
											</td>
											<td>
												Used as a 2-pin programmer board for uploading the code to the main
												ATtiny 412 board through it.
											</td>
										</tr>
										<tr>
											<th scope="row">4</th>
											<td>USB Extension Cable</td>
											<td>
												<a href="https://www.amazon.com/AmazonBasics-Extension-Cable-Male-Female/dp/B00NH11R3I">
													Amazon
												</a>
											</td>
											<td>Used for connecting to the USB UPDI FT230XS Programmer.</td>
										</tr>
										<tr>
											<th scope="row">5</th>
											<td>USB Hub</td>
											<td>
												<a href="https://www.amazon.com/-/zh_TW/dp/B00L2442H0/ref=sr_1_3?dchild=1&keywords=USB+hub&qid=1587914225&s=electronics&sr=1-3">
													Amazon
												</a>
											</td>
											<td>-</td>
										</tr>
									</tbody>
								</Table>
							</ListGroupItem>
							<ListGroupItem action>
								<ListGroupItemHeading id="datasheet">Read a Microcontroller Datasheet</ListGroupItemHeading>
								<p>
									From the{' '}
									<a href="http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf">
										ATtiny 412 datasheet
									</a>, I learnt that each pin has different functions (e.g. in addtion to normal I/O, <code>PA1</code> and <code>PA2</code> can also be used as{' '}
									<code>TX</code> and <code>RX</code> for serial communication with a computer. <code>PA0</code> can be connected to {' '}
									a <code>UPDI</code> programmer for uploading a <code>hex</code> file.) With the code below,{' '}
									the ATtiny 412 board can read the input text from <code>Arduino Serial Monior</code> through a <code>FTDI</code> cable or host board withouting connecting any other I/O devices.
								</p>
								<Row className="mb-3">
									<img alt='' src="../../08-WeekEight/echo1.png"/>
								</Row>
								<Row className="mb-3">
									<img alt='' src="../../08-WeekEight/echo2.png"/>
								</Row>
								<a href="https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_echo">
									ATtiny412_echo.ino
								</a>
								<pre>
									{attiny412_echo}
								</pre>
								<p>
									The datasheet also shows how to control the pins in a faster way {' '}
									by setting certain port registers to <code>0 (Low)</code> or <code>1 (High)</code>.
								</p>
								<h5 className='mt-4'>Port Manipulation Comparison</h5>
								<p>Here I use <code>PA3</code> for example.</p>
								<Table bordered className="mb-5">
									<thead>
										<tr>
											<th>Function</th>
											<th>Register</th>
											<th>Arduino</th>
										</tr>
									</thead>
									<tbody>
										<tr>
										<th scope="row">Output</th>
											<td>
												PORTA.DIRSET = PIN3_bm;<br/>
												PORTA.OUTSET = PIN3_bm;
											</td>
											<td>pinMode(4, OUTPUT);</td>
										</tr>
										<tr>
										<th scope="row">Input</th>
											<td>PORTA.DIRCLR = PIN3_bm;</td>
											<td>pinMode(4, INPUT);</td>
										</tr>
										<tr>
											<th scope="row">High</th>
											<td>PORTA.OUT |= PIN3_bm;</td>
											<td>digitalWrite(4, HIGH);</td>
										</tr>
										<tr>
											<th scope="row">Low</th>
											<td>PORTA.OUT &= ~PIN3_bm;</td>
											<td>digitalWrite(4, LOW);</td>
										</tr>
									</tbody>
									</Table>

								<p>
									There are two ways to specify the ATtiny 412 pins in Arduino IDE. One is to use their original names shown below:
								</p>

								<h5 className='mt-2'>ATtiny 412 Pins</h5>
								<Row>
									<Col lg="4"><img alt='' src="../../08-WeekEight/ATtiny412_pins.png" /></Col>
									<Col lg="8"><img alt='' src="../../08-WeekEight/ATtiny412_pin_table.png" /></Col>
								</Row>
								<a href="https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink">
									ATtiny412_blink.ino
								</a>
								<pre>
									{attiny412_blink}
								</pre>

								<p className='mt-5'>
									The other is to map the ATtiny 412 pins to the Arduino pin numbers{' '}
									with the <a href="https://github.com/SpenceKonde/megaTinyCore">megaTinyCore</a> library installed{' '}
									so that we can simplify the code shown below:
								</p>
								<h5 className='mt-2'>ATtiny 412 Pins (mapped to Arduino pin numbers)</h5>
								<img alt='' src='../../08-WeekEight/ATtiny412_x12_pin.gif' />
								<a href="https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink_simplified">
									ATtiny412_blink_simplified.ino
								</a>
								<pre>
									{attiny412_blink_simplified}
								</pre>
							</ListGroupItem>
							<ListGroupItem action>
								<ListGroupItemHeading>
									Set up Programming Environment (Arduino IDE/megaTinyCore version)
								</ListGroupItemHeading>
								<h5 className="mt-3">
									Step 1. Connect both boards to a USB hub and install both{' '}
									<a href="https://www.ftdichip.com/Drivers/VCP.htm">VCP</a> and{' '}
									<a href="https://www.ftdichip.com/Drivers/D2XX.htm">D2XX</a> drivers for a
									computer to detect FTDI devices (p.s. <a href="https://www.wacom.com/en-us/support/product-support/drivers">Wacom drivers</a> might affect the FTDI detection.)
								</h5>
								<Row>
									<Col sm="12" lg={{ size: 8, offset: 2 }}>
										<img alt='' src="../../08-WeekEight/board_ports.jpg" />
									</Col>
								</Row>
								<h5 className="mt-5" id="port">
									Step 2. Type <code>ls /dev/tty.*</code> command in the terminal to find out both
									FTDI and UPDI ports
								</h5>
								<p>Here:</p>
								<p className="mb-0">
									FTDI Host Board
								</p>
								<pre>/dev/tty.usbserial-D3072T22</pre>
								<p className="mb-0">
									UPDI Programmer Board
								</p>
								<pre>/dev/tty.usbserial-D3072T1T</pre>
								<Row>
									<Col sm="12" lg={{ size: 8, offset: 2 }}>
										<img alt='' src="../../08-WeekEight/FTDI_UPDI_ports.png" />
									</Col>
								</Row>
								<h5>
									Step 3. Install{' '}
									<a href="https://github.com/SpenceKonde/megaTinyCore">megaTinyCore</a> Arduino
									library
								</h5>
								<p>
									Copy and paste this URL{' '}
									<code>http://drazzy.com/package_drazzy.com_index.json</code> from{' '}
									<a href="https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md">
										Boards Manager Installation
									</a>{' '}
									section of the megaTinyCore library to the Arduino preference.
								</p>
								<Row>
									<Col sm="12" lg={{ size: 8, offset: 2 }}>
										<img alt='' src="../../08-WeekEight/Arduino_preference.png" />
									</Col>
								</Row>
								<p>
									After that, type the <code>megaTinyCore</code> keyword in the <code>Tools -> Board -> Board Manager</code>{' '}
									and install it.
								</p>
								<Row>
									<Col sm="12" lg={{ size: 8, offset: 2 }}>
										<img alt='' src="../../08-WeekEight/Arduino_board_manager.png" />
									</Col>
								</Row>
								<h5 className="mt-5">
									Step 4. Add the <code>build path</code> to the Arduino preference.txt for saving
									the compiled <code>hex</code> file
								</h5>
								<p>
									Click the link shown in the Arduino preference to enter the{' '}
									<code>preference.txt</code> and add the following new line.
								</p>
								<Row>
									<Col sm="12" lg={{ size: 8, offset: 2 }}>
										<img alt='' src="../../08-WeekEight/Arduino_add_build_path.png" />
									</Col>
								</Row>
								<h5 className="mt-5">
									Step 5. Restart Arduino IDE and choose which type of the ATtiny boards you want to program in the
									tool menu and its settings to start programming
								</h5>
								<Row>
									<Col md="0" lg="2"></Col>
									<Col md="6" lg="4">
										<img alt='' src="../../08-WeekEight/Arduino_board.png" />
									</Col>
									<Col md="6" lg="4">
										<img alt='' src="../../08-WeekEight/ATtiny412_setting.png" />
									</Col>
									<Col md="0" lg="2"></Col>
								</Row>
								<h5 className="mt-5">
									Step 6. Start programming the code in Arudino IDE and compile it to generate a{' '}
									<code>hex</code> file in the <code>build</code> subfolder
								</h5>
								<p>
									<Link to="#datasheet">
										How to control the I/O of an ATtiny 412 chip by programming its registers
										can be found in the previous datasheet section.
									</Link>
								</p>
								<p>
									After finishing coding, click the verify button to comile the <code>hex</code>{' '}
									file which will be uploaded via <code>pyupdi</code> libray in the terminal.
								</p>
								<Row>
									<Col>
										<img alt='' src="../../08-WeekEight/Arduino_hex.png" />
									</Col>
								</Row>
								<h5 className="mt-5">
									Step 7. Install{' '}
									<a href="https://realpython.com/intro-to-pyenv/">Python3 with pyenv</a>
								</h5>
								<p>
									pyenv is a wonderful tool for managing multiple Python versions. Even if you
									already have Python installed on your system, it is worth having pyenv installed
									so that you can easily try out new language features or help contribute to a
									project that is on a different version of Python.
								</p>
								<p>Follow the order below to install python properly:</p>
								<p>
									<kbd>git clone https://github.com/pyenv/pyenv.git ~/.pyenv</kbd> : download
									pyenv from GitHub
								</p>
								<p>
									<kbd>echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc</kbd> : assign its
									envrionment paths in the <code>.zshrc</code> (or <code>.bashrc</code>) profile
								</p>
								<p>
									<kbd>echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc</kbd>
								</p>
								<p>
									<kbd>
										echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init
										-)"\nfi ' >> ~/.zshrc
									</kbd>
								</p>
								<p>
									<kbd>exec "$SHELL"</kbd>
								</p>
								<p>
									<kbd>pyenv install 3.7.7</kbd> : install the specific python version you
									prefer (Here: 3.7.7)
								</p>
								<p>
									<kbd>pyenv global 3.7.7</kbd> : switch to the installed version (Here: 3.7.7)
								</p>
								<p>Other useful pyenv commands:</p>
								<p>
									<kbd>pyenv versions</kbd> : list all the installed versions
								</p>
								<p>
									<kbd>pyenv version</kbd> : list the version in use
								</p>
								<p>
									<kbd>pyenv uninstall 3.7.7</kbd> : uninstall certain version (Here: 3.7.7)
								</p>
								<Row>
									<Col sm="12" lg={{ size: 8, offset: 2 }}>
										<img alt='' src="../../08-WeekEight/pyenv.png" />
									</Col>
								</Row>
								<h5 className="mt-5">
									Step 8. Install <a href="https://github.com/pyserial/pyserial">pyserial</a>{' '}
									and <a href="https://github.com/mraardvark/pyupdi">pyupdi</a> packages via{' '}
									<a href="https://pip.pypa.io/en/stable/reference/pip_install/">pip</a> command{' '}
								</h5>
								<p>
									<kbd>pip install pyserial</kbd> : install the pyserial package
								</p>
								<p>
									<kbd>pip install https://github.com/mraardvark/pyupdi/archive/master.zip</kbd> :
									install the pyupdi package
								</p>
								<p>
									<kbd>pyupdi</kbd> : check pyupdi usage
								</p>
								<p>
									<kbd>pip freeze</kbd> : list all the installed packages
								</p>
								<p>
									<kbd>pip freeze | grep pyserial</kbd> : list the certain installed package (Here:
									pyserial)
								</p>
								<Row>
									<Col sm="12" lg={{ size: 8, offset: 2 }}>
										<img alt='' src="../../08-WeekEight/pip.png" />
									</Col>
								</Row>
								<h5 className="mt-5">
									Step 9. Upload the compiled hex file to the ATtiny 412 board via{' '}
									<a href="https://github.com/mraardvark/pyupdi">pyupdi</a>
								</h5>
								<p>
									<kbd>ls /dev/tty.*</kbd> : find out which port is the UPDI programmer
									board (Here: <Link to="#port">/dev/tty.usbserial-D3072T1T</Link>)
								</p>
								<p>
									<kbd>
										pyupdi -d tiny412 -b 115200 -c /dev/tty.usbserial-D3072T1T -f
										/Users/USER_NAME/Documents/Arduino/build/ATtiny412_blink.ino.hex -v
									</kbd>{' '}
									: upload the compiled <a href="https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink">code</a> to the ATtiny 412 board with 115200 baud rate
								</p>
								<Row>
									<Col sm="12" lg={{ size: 8, offset: 2 }} className="mb-3">
										<video width="100%" controls>
											<source
												src="../../08-WeekEight/pyupdi_upload_code.mp4"
												type="video/mp4"
											/>
										</video>
									</Col>
								</Row>
							</ListGroupItem>
							<ListGroupItem action>
								<ListGroupItemHeading>End Result</ListGroupItemHeading>
								<p>The board I redrew finally works after I programmed it.</p>
								<Row className="mb-3">
									<Col lg="6" className="mb-3">
										<a href="https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink">
											ATtiny412_blink.ino
										</a>: blink the LED
										<video width="100%" controls>
											<source
												src="../../06-WeekSix/ATtiny412_blink-small.mp4"
												type="video/mp4"
											/>
										</video>
									</Col>
									<Col lg="6" className="mb-3">
									<a href="https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink_button">
											ATtiny412_blink_button.ino
										</a>: blink the LED if the button is pressed
										<video width="100%" controls>
											<source
												src="../../06-WeekSix/ATtiny412_button-small.mp4"
												type="video/mp4"
											/>
										</video>
									</Col>
								</Row>
							</ListGroupItem>
						</ListGroup>
					</Col>
				</Row>
			</Container>
			<hr />
			<Container>
				<Row>
					<Col className="h3">Group Assignment: Compare the performance and development workflows between different AVR boards</Col>
				</Row>
				<Row>
					<Col>
						<ListGroup>
							<ListGroupItem action>
								<ListGroupItemHeading>
									Program Size Comparison
								</ListGroupItemHeading>
								<p>
									After finishing soldering, I used a multimeter and switched it to the{' '}
									<code>buzzing mode</code> which sounds if two points are connected (<code>0Ω</code>{' '}
									between them). In order to find out how much current does each LED consume, I
									then measured their cross voltage by switching to the <code>DC voltage</code>{' '}
									mode:
								</p>
								<Row>
									<Col>
										<a href="https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink">
										ATtiny412_blink.ino
										</a>
										<img alt='' src="../../08-WeekEight/ATtiny412_blink.png" />
									</Col>
									<Col>
										<a href="https://gitlab.com/cv47522/avr-code/-/tree/master/ATtiny/ATtiny412_blink_simplified">
										ATtiny412_blink_simplified.ino
										</a>
										<img alt='' src="../../08-WeekEight/ATtiny412_blink_simplified.png" /></Col>
									<Col>Arduino Uno
										<img alt='' src="../../08-WeekEight/Arduino_blink.png" />
									</Col>
								</Row>
							</ListGroupItem>
						</ListGroup>
					</Col>
				</Row>
			</Container>
		</div>
	</Layout>
  );
}

