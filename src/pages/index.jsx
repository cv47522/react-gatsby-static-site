import React from 'react';

import Layout from '../components/layout';
import HomeCards from '../components/HomeCards';

import '../styles/font-awesome.min.css';
import '../styles/bootstrap.min.css';
import '../styles/index.css';


export default function Main({ location }) {
  return (
    <div>
      <Layout>
        {/* <h1>The location is {location.pathname}</h1> */}
        <HomeCards />
      </Layout>
    </div>
  );
}
