import React from 'react';
import Layout from '../components/layout';
import WeekBanner from '../components/WeekBanner';
import { Link } from "gatsby";
import {
  Container, Row, Col,
  ListGroup, ListGroupItem,
  ListGroupItemHeading, ListGroupItemText }
from 'reactstrap';

export default function WeekNine() {
  return (
    <Layout>
      <WeekBanner weekId='9' />
      <div>
        <Container>
          <Row>
            <Col className='h3'>3D Modelling Project</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Design Tool: <a href='https://www.autodesk.com/products/fusion-360/overview'>Fusion 360</a></ListGroupItemHeading>
                  <p>
                    Since Fusion 360 is an integrated CAD, CAM, and CAE software, which makes it easier to design both 2D and 3D models within the same platform.
                    For this week, I designed a hex-shaped pedal holder which is one of the components I am going to apply for my <Link to ='/week/18'>final project</Link>.
                  </p>
                  <img alt='' className='mb-3' src='../../02-WeekTwo/00_model.png' />
                </ListGroupItem>
                <ListGroupItem action>
                  <ListGroupItemHeading>Design Process</ListGroupItemHeading>
                  <p>
                    The following steps indicate how I created this 3D model:
                  </p>
                  <h6>Step 1. Open "change parameters" dialogue</h6>
                  <a href='../../02-WeekTwo/01_change_params.png'><img alt='' className='mb-4' src='../../02-WeekTwo/01_change_params.png' /></a>
                  <h6>Step 2. Input user-defined parameters</h6>
                  <a href='../../02-WeekTwo/02_params.png'><img alt='' className='mb-4' src='../../02-WeekTwo/02_params.png' /></a>
                  <h6>Step 3. In sketch mode, create a circumscribed polygon</h6>
                  <a href='../../02-WeekTwo/03_sketch_polygon.png'><img alt='' className='mb-4' src='../../02-WeekTwo/03_sketch_polygon.png' /></a>
                  <Row className='mb-3'>
                    <Col lg='6' className='mb-'><h6>Step 4. Assign the polygon type(ex. pentagon, hexagon or octagon)</h6>
                      <a href='../../02-WeekTwo/04_sketch_create_polygon.png'><img alt='' src='../../02-WeekTwo/04_sketch_create_polygon.png' /></a>
                    </Col>
                    <Col lg='6' className='mb-'><h6>Step 5. Input the user-defined parameter to its dimension</h6>
                      <a href='../../02-WeekTwo/05_sketch_hex_polygon.png'><img alt='' src='../../02-WeekTwo/05_sketch_hex_polygon.png' /></a>
                    </Col>
                  </Row>
                  <Row className='mb-3'>
                    <Col lg='6' className='mb-'><h6>Step 6. Offset the hexagon sketch</h6>
                      <a href='../../02-WeekTwo/06_sketch_offset.png'><img alt='' src='../../02-WeekTwo/06_sketch_offset.png' /></a>
                    </Col>
                    <Col lg='6' className='mb-'><h6>Step 7. Finish the hexagon sketch</h6>
                      <a href='../../02-WeekTwo/07_sketch_finish_hex.png'><img alt='' src='../../02-WeekTwo/07_sketch_finish_hex.png' /></a>
                    </Col>
                  </Row>
                  <h6>Step 8. Extrude the hexagon sketch</h6>
                  <a href='../../02-WeekTwo/08_solid_extrude_hex.png'><img alt='' className='mb-4' src='../../02-WeekTwo/08_solid_extrude_hex.png' /></a>
                  <h6>Step 9. Switch to the front view then create a new sketch with two circles and a rectangle</h6>
                  <a href='../../02-WeekTwo/09_sketch_front_circle.png'><img alt='' className='mb-4' src='../../02-WeekTwo/09_sketch_front_circle.png' /></a>
                  <h6>Step 10. Project the hexagon body to the new sketch for easily creating its constrains</h6>
                  <a href='../../02-WeekTwo/10_sketch_project.png'><img alt='' className='mb-4' src='../../02-WeekTwo/10_sketch_project.png' /></a>
                  <h6>Step 11. Extrude the new sketch which forms a lower tube</h6>
                  <a href='../../02-WeekTwo/11_solid_extrude_tube.png'><img alt='' className='mb-4' src='../../02-WeekTwo/11_solid_extrude_tube.png' /></a>
                  <h6>Step 12. Use the circular pattern function to duplicate the lower tube</h6>
                  <a href='../../02-WeekTwo/12_solid_circular_pattern.png'><img alt='' className='mb-4' src='../../02-WeekTwo/12_solid_circular_pattern.png' /></a>
                  <h6>Step 13. Select the features for duplicate and assign the quantity</h6>
                  <a href='../../02-WeekTwo/13_solid_duplicate_tube.png'><img alt='' className='mb-4' src='../../02-WeekTwo/13_solid_duplicate_tube.png' /></a>
                  <h6>Step 14. Switch to the top view then create a new sketch with a rectangle</h6>
                  <a href='../../02-WeekTwo/14_sketch_rectangle.png'><img alt='' className='mb-4' src='../../02-WeekTwo/14_sketch_rectangle.png' /></a>
                  <h6>Step 15. Extrude the rectangle sketch</h6>
                  <a href='../../02-WeekTwo/15_solid_extrude_rectangle.png'><img alt='' className='mb-4' src='../../02-WeekTwo/15_solid_extrude_rectangle.png' /></a>
                  <h6>Step 16. Create a new sketch with a circle on the ectruded rectangle solid</h6>
                  <a href='../../02-WeekTwo/16_sketch_hole.png'><img alt='' className='mb-4' src='../../02-WeekTwo/16_sketch_hole.png' /></a>
                  <h6>Step 17. Extrude the circle as a hole by cutting the cube</h6>
                  <a href='../../02-WeekTwo/17_solid_extrude_hole.png'><img alt='' className='mb-4' src='../../02-WeekTwo/17_solid_extrude_hole.png' /></a>
                  <h6>Step 18. Add fillets to smooth the edges of the upper tube</h6>
                  <a href='../../02-WeekTwo/18_solid_add_fillet.png'><img alt='' className='mb-4' src='../../02-WeekTwo/18_solid_add_fillet.png' /></a>
                  <h6>Step 19. Use the circular pattern function again to duplicate the upper tube by selecting the features and assigning the quantity</h6>
                  <a href='../../02-WeekTwo/19_solid_duplicate_tube_v2.png'><img alt='' className='mb-4' src='../../02-WeekTwo/19_solid_duplicate_tube_v2.png' /></a>
                  <h6>Step 20. Finished!</h6>
                  <a href='../../02-WeekTwo/00_model.png'><img alt='' className='mb-4' src='../../02-WeekTwo/00_model.png' /></a>
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
      </div>
    </Layout>
  );
}

