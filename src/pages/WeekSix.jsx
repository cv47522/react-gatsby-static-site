import React from 'react';
import { Link } from "gatsby";
import Layout from '../components/layout';
import WeekBanner from '../components/WeekBanner';
import {
  Container, Row, Col, Table,
  ListGroup, ListGroupItem,
  ListGroupItemHeading, ListGroupItemText }
from 'reactstrap';

export default function WeekSix() {
  return (
    <Layout>
      <WeekBanner weekId='6' />
      <div className='mb-5'>
        <Container className='pb-5'>
          <Row>
            <Col className='h3'>Individual PCB Design Project: ATtiny 412 Double-Sided Board</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Materials</ListGroupItemHeading>
                    <Table striped responsive id='material-table'>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Digi-Key Part No.</th>
                        <th>Name</th>
                        <th>Value * Pieces</th>
                        <th>KiCad Footprint(Package)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>ATtiny 412-SSFRCT-ND</td>
                        <td>ATtiny 412 w/ Single-pin UPDI (<a href='http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf'>Datasheet</a>)</td>
                        <td>tinyAVR™ 1 Microcontroller IC 8-Bit 20MHz 4KB FLASH 8-SOIC * 1</td>
                        <td>Package_SO: SOIC-8_3.9x4.9mm_P1.27mm</td>
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>B3SN-3112P</td>
                        <td>B3SN Tactile Switch, SMD</td>
                        <td> * 1</td>
                        <td>self-designed footprint: TACTILE_SW_B3S</td>
                      </tr>
                      <tr>
                        <th scope="row">3</th>
                        <td><ul className="pl-3">
                          <li>160-1889-1-ND</li>
                          <li>160-1403-1-ND</li>
                        </ul></td>
                        <td>LED 1206 SMD</td>
                        <td><ul className="pl-3">
                          <li>BLUE CLEAR * 1</li>
                          <li>YELLOW ORANGE * 1</li>
                        </ul></td>
                        <td>LED_SMD: LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder</td>
                      </tr>
                      <tr>
                        <th scope="row">4</th>
                        <td><ul className="pl-3">
                          <li>311-499FRCT-ND</li>
                          <li>311-10.0KFRCT-ND</li>
                        </ul></td>
                        <td>RES SMD 1% 1/4W 1206</td>
                        <td><ul className="pl-3">
                          <li>499 Ω * 2</li>
                          <li>10k Ω * 1</li>
                        </ul></td>
                        <td>Resistor_SMD: R_1206_3216Metric_Pad1.42x1.75mm_HandSolder</td>
                      </tr>
                      <tr>
                        <th scope="row">5</th>
                        <td>445-1423-1-ND</td>
                        <td>CAP CER 1UF 50V X7R 1206</td>
                        <td>1 uF * 1</td>
                        <td>Capacitor_SMD: C_1206_3216Metric_Pad1.42x1.75mm_HandSolder</td>
                      </tr>
                      <tr>
                        <th scope="row">6</th>
                        <td>445-1423-1-ND</td>
                        <td>Male 1 Row Horizontal SMD Header 2.54MM</td>
                        <td><ul className="pl-3">
                          <li>Conn_01x06_Male * 1</li>
                          <li>Conn_01x02_Male * 1</li>
                        </ul></td>
                        <td>self-designed footprint:
                          <ul className="pl-3">
                            <li>FTDI_Header_1x06_P2.54mm_Horizontal</li>
                            <li>UPDI_Header_1x02_P2.54mm_Horizontal</li>
                          </ul>
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                  </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Design Tool: <a href='https://www.kicad-pcb.org/'>KiCad EDA</a></ListGroupItemHeading>
                    <p>
                      <a href='https://www.kicad-pcb.org/'>KiCad EDA</a> is a cross platform and open source electronics design automation suite with lots of predefined symbol and footprint libraries provided by industrial companies such as <a href='https://www.digikey.com/en/resources/design-tools/kicad'>Digi-Key</a> and <a href='https://github.com/XenGi/teensy_library'>Teensy</a>. I used it to design my PCB by redrawing the original <a href='http://academy.cba.mit.edu/classes/embedded_programming/index.html'>hello.t412.echo board</a> and adding two extra LEDS(a power LED and a programmable LED) as well as a push-button.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><h6>Original hello.t412.echo Board</h6><a href='../../06-WeekSix/hello.t412.echo.png'><img alt='' src='../../06-WeekSix/hello.t412.echo.png' /></a></Col>
                      <Col lg='6' className='mb-3'><h6>My Board</h6><a href='../../06-WeekSix/hello.t412.LED.Button.png'><img alt='' src='../../06-WeekSix/hello.t412.LED.Button.png' /></a></Col>
                    </Row>
                  </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Schematic Design</ListGroupItemHeading>
                    <img alt='' className='mb-3' src='../../06-WeekSix/Sch_mode.png' />
                    <p>
                      Since I added the following components to my new board, it is common to add an extra resistor between the I/O pin of the ATtiny 412 chip or Vcc(input power) and the component itself to make sure that the current is not too high to break them:
                    </p>
                      <ul className='mb-3'>
                        <li>Power LED</li>
                        <li>Programmable LED</li>
                        <li>Push-Button</li>
                      </ul>
                      <Row className='mb-3'>
                        <Col lg='8'><img alt='' src='../../06-WeekSix/hero-shot_blink.jpg' /></Col>
                      </Row>
                    <h5 className='mt-5'>Decide the Current-Limiting Resistor for LED</h5>
                    <img alt='' className='w-75' src='../../06-WeekSix/Sch_pwr_LED.png' />
                    <p>
                      There are two main rules which I learnt from <a href='https://learn.adafruit.com/all-about-leds/forward-voltage-and-kvl'>Adafruit's Forward Voltage and KVL article</a> to follow in order to design the correct circuit for the LED connection:
                    </p>
                    <ul className='mb-3'>
                      <li>
                        <a href='https://en.wikipedia.org/wiki/Kirchhoff%27s_circuit_laws'>Kirchhoff's Voltage Law(KVL)</a>: In any 'loop' of a circuit, the voltages must balance, that is <code>the amount generated = the amount used</code>
                        <Row className='mb-3'>
                          <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/leds_kvl.gif' /></Col>
                          <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/leds_resistordrop.gif' /></Col>
                        </Row>
                      </li>
                      <li><a href='https://en.wikipedia.org/wiki/Ohm%27s_law'>Ohm's Law</a>: V = I * R (V: voltage, R: resistance, I: current)</li>
                    </ul>
                    <p>
                      First, I tried to find the forward voltage of the LED I used from its <a href='https://media.digikey.com/pdf/Data%20Sheets/Lite-On%20PDFs/LTST-C230TBKT.pdf'>datasheets</a> which indicates that its forward voltage(V<sub>F</sub>) and forward current(I<sub>F</sub>) are around <code>3V</code> and <code>20mA</code> respectively,
                      which means that I need to make sure that the cross-voltage of the LED cannot exceed 3.8V or it will burn down the LED.
                    </p>
                    <p>
                      In other words, the resistor used to limit the current to 20mA should consume <code>the remaining 2V (Vcc - V<sub>F</sub>, Vcc=5V from USB, V<sub>F</sub>=3V)</code>.
                    </p>
                    <p>
                      Then, I put these values <code>(Vcc=5V, V<sub>F</sub>=3V, I<sub>F</sub>=20mA)</code> into the equation to get the correct resistance:
                    </p>
                    <pre>
                      <h6>
                        Voltage across a resistor (volts) = Current through the resistor (amperes)* The Resistance of the resistor (ohms)
                      </h6>
                      <h6 className='font-weight-bold'>
                        → Vcc - V<sub>F</sub> = I<sub>F</sub> * R
                      </h6>
                      <h6 className='font-weight-bold'>
                        → 5V - 3V = 0.02A(= 20mA) * R
                      </h6>
                      <h6 className='font-weight-bold'>
                        → R = 100 Ω (the minimum value)
                      </h6>
                  </pre>
                    <img alt='' className='mb-3' src='../../06-WeekSix/LED_forward_V.png' />
                    <p>
                      According the answer, I chose 499Ω which is above the 100Ω (the minimum value) as the resistance to form the circuit for both the power LED and the programmable one.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='8'><img alt='' src='../../06-WeekSix/Sch_LEDs.png' /></Col>
                    </Row>
                    <h5 className='mt-5'>ATtiny 412 Pins</h5>
                    <p>
                      In order to program the self-designed board, I need to understand <a href='https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md'>the function of each pin of the ATtiny 412 chip</a>.
                      Since ATtiny 412 has an Unified Program Debug Interface (UPDI) pin which is <code>PA0</code> pin, it is easier to program it with a 1x2 male pin header(<code>J1: Conn_01x02_Male</code>).
                    </p>
                    <img alt='' src='../../06-WeekSix/ATtiny_412.gif' />
                    <p>
                      The other 1x6 male pin header(<code>J2: Conn_01x06_Male</code>) is connected to a FTDI cable or <a href='https://gitlab.com/aaltofablab/usb-ftdi-ft230xs'>FTDI progremmer board</a> to power on and read data from the ATtiny 412 board by communicating through RX(<code>PA1</code>) and TX(<code>PA2</code>) pins.
                    </p>
                    <a href='../../06-WeekSix/ATtiny412_sch.png'><img alt='' src='../../06-WeekSix/Sch_ATtiny412.png' /></a>
                    <p>
                      For the other three remaining port pins, I used two of them as IO pins to control the programmable LED(<code>PA3</code>) and the push-button(<code>PA6</code>) respectively.
                      Since I am not going to use <code>PA7</code> and the other two pins of the FTDI male connector, I placed a no connection flag to it to make sure it does not work against the electrical rules while I check the performance by clicking <img alt='' className='w-5' src='../../06-WeekSix/btn_debug.png' /> button.
                    </p>
                    <p>
                      Besides, it is also a good habit to put a <a href='http://blog.optimumdesign.com/how-to-place-a-pcb-bypass-capacitor-6-tips'>bypass capacitor</a> between <code>Vcc</code> and <code>GND</code> pins(power pins) of the chip.
                    </p>
                    <img alt='' className='mb-3' src='../../06-WeekSix/Sch_LED_button_IO.png' />
                    <h5 className='mt-5'>Annotate Schematic Symbols</h5>
                    <p>
                      In order to specify different components, I then opend the <code>Annotate Schematic</code> dialogue by clicking <img alt='' className='w-5' src='../../06-WeekSix/btn_annotate.png' /> button to lable them with non-repetitive numbers.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='8'><img alt='' src='../../06-WeekSix/Sch_annotate.png' /></Col>
                    </Row>
                    <h5 className='mt-5'>Assign Footprint</h5>
                    <p>
                      Before layouting the printed circuit board, I need to assign which footprint belongs to which symbol(component) in the <code>Assign Footprint</code> dialogue by clicking <img alt='' className='w-5' src='../../06-WeekSix/btn_assign_footprint.png' /> button to open it.
                    </p>
                    <img alt='' className='mb-3' src='../../06-WeekSix/Sch_assign_footprint.png' />
                    <p>
                      The name of each correspondent footprint is also arrranged in the <Link to="#material-table"><code>material table</code></Link> shown on top of the page.
                    </p>
                    <h5 className='mt-5'>Generate Netlist</h5>
                    <p>
                      The final step of the schematic design is to generate the netlist file which will then be imported to the PCB layout mode.
                      I did this by clicking <img alt='' className='w-5' src='../../06-WeekSix/btn_generate_netlist.png' /> button to open the <code>Generate Netlist</code> dialogue to generate and save the netlist file.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='8'><img alt='' src='../../06-WeekSix/Sch_generate_netlist.png' /></Col>
                    </Row>
                    <h5 className='mt-5'>Complete Schematic</h5>
                    <img alt='' className='mb-3' src='../../06-WeekSix/Sch_board.png' />
                  </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Footprint Layout</ListGroupItemHeading>
                    <p>
                      After finishing the schematic design, I then switched to the PCB layout mode via <img alt='' className='w-5' src='../../06-WeekSix/btn_PCB.png' /> button.
                    </p>
                    <img alt='' className='mb-3' src='../../06-WeekSix/PCB_mode.png' />
                    <h5 className='mt-5'>Import Netlist File</h5>
                    <p>
                      In the PCB mode, I clicked <img alt='' className='w-5' src='../../06-WeekSix/btn_generate_netlist.png' /> again to load the netlist file I saved.
                    </p>
                    <img alt='' src='../../06-WeekSix/PCB_load_netlist.png' />
                    <p>These are some messy nets which I am going to layout:</p>
                    <img alt='' className='mb-3' src='../../06-WeekSix/PCB_messy_net.png' />
                    <img alt='' className='mb-3' src='../../06-WeekSix/PCB_after_layout.png' />
                    After layouting all the components, I started to create traces between them by using <img alt='' className='w-5' src='../../06-WeekSix/btn_add_wires.png' /> or typing its shortcut <code>W</code>.
                    If there is not enough space for routing tracks, I then created <a href='https://en.wikipedia.org/wiki/Via_(electronics)'>vias</a> which are conductive holes by using <img alt='' className='w-5' src='../../06-WeekSix/btn_add_vias.png' /> or typing its shortcut <code>V</code> to make traces first go to the other side then come back to the original side for its connection:
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_add_wires.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_add_vias.png' /></Col>
                    </Row>
                    <p>
                      Since there are many pins connect to <code>GND</code>, it is also useful to create a ground plane on the other side by switching to <code>B.Cu</code> layer and using <img alt='' className='w-5' src='../../06-WeekSix/btn_add_fill_zone.png' /> <code>Add fill zones</code> tool to first frame the area then assign it as a <code>GND plane</code>.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_copper_zone.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_add_fill_zone.png' /></Col>
                    </Row>
                    <p>
                      The final step of the PCB layout is to define its outline for <a href='http://mods.cba.mit.edu/'>mods</a> to generate its interior toolpath. To do that, I first switched to the <code>Edge.Cuts</code> layer and used <img alt='' className='w-5' src='../../06-WeekSix/btn_add_lines.png' /> <code>Add graphic lines</code> tool to draw a rectangle area
                      and then switched to the <code>Dwgs.User</code> layer to fill the field with <img alt='' className='w-5' src='../../06-WeekSix/btn_add_polygon.png' /> <code>Add graphic polygon</code> tool.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_layer_Edge.Cuts.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_layer_Dwgs.User.png' /></Col>
                    </Row>
                    <p>
                      Finally, I added a margin rectangle to the <code>Margin</code> layer which is reserved for the drawing of circuit board outline. Any element (graphic, texts…) placed on this layer appears on all the other layers.
                      It also defines the <code>board area</code> for exporting the image for each layer.
                    </p>
                    <h5 className='mt-5'>Complete PCB View</h5>
                    <img alt='' className='mb-3' src='../../06-WeekSix/PCB_board.png' />
                  </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Export PCB Svg</ListGroupItemHeading>
                    <p>
                      Before starting milling the board, we need to generate the image files for its <code>front-side traces, back-side traces, drilling and outline</code>.
                      I did this with the following steps and settings:
                    </p>
                    <Row className='mb-3'>
                      <Col lg='8' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_export_svg.png' /></Col>
                    </Row>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_export_F.Cu.png' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/PCB_export_B.Cu.png' /></Col>
                    </Row>
                    <p>
                      For the <code>drilling svg</code>, I filtered out the vias from the <code>F.Cu.svg</code> and infilled them with black color in <a href='https://inkscape.org/'>Inkscape</a>:
                    </p>
                    <img alt='' className='mb-3' src='../../06-WeekSix/Inkscape_fill_color.png' />
                  </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Fabrication: SRM-20 Milling Machine</ListGroupItemHeading>
                    <p>
                      The fabrication of the PCB is almost the same as the process of <Link to ='/week/4'>Week 4: Electronics Production</Link> except the server module used to generate the cutting toolpath in <a href='http://mods.cba.mit.edu/'>mods</a> is:
                    </p>
                    <img alt='' className='w-15 mb-3' src='../../06-WeekSix/mods_PCB_svg.png' />
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'>
                        <h6>Front Side:
                          <span className='font-weight-normal'>
                            <a href='../../06-WeekSix/source_files/0305-ATtiny412_LED_Button/srm-20_mill_files/0305-elec-design-F_Cu.svg'>F_Cu.svg</a>(0.3mm),
                            <a href='../../06-WeekSix/source_files/0305-ATtiny412_LED_Button/srm-20_mill_files/0305-elec-design-F_Drills.svg'>F_Drills.svg</a> & <a href='../../06-WeekSix/source_files/0305-ATtiny412_LED_Button/srm-20_mill_files/0305-elec-design-Dwgs_User.svg'>Dwgs_User.svg</a>(0.8mm)
                          </span>
                        </h6><img alt='' src='../../06-WeekSix/F_cu.jpg' />
                      </Col>
                      <Col lg='6' className='mb-3'>
                        <h6>Back Side:
                          <span className='font-weight-normal'>
                            <a href='../../06-WeekSix/source_files/0305-ATtiny412_LED_Button/srm-20_mill_files/0305-elec-design-B_Cu.svg'>B_Cu.svg</a>(0.4mm )
                          </span>
                        </h6><img alt='' src='../../06-WeekSix/B_cu.jpg' />
                      </Col>
                    </Row>
                    <h5 className='mt-5'>Fill Holes with <a href='https://en.wikipedia.org/wiki/Via_(electronics)'>Vias</a> to Connect Both Sides</h5>
                    <p>
                      Before soldering the PCB, I first inserted the <code>copper hollow rivets</code> <img alt='' className='w-10' src='../../06-WeekSix/rivets.jpg' /> to make traditional vias by using a hammer and its flanging tools which are a <a href='https://www.mcmaster.com/3498a11'>marking punch</a> and a <a href='https://www.mcmaster.com/3415a13'>pin-loosening punch</a>.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='4' className='mb-3'><img alt='' className='mb-3' src='../../06-WeekSix/vias_rivets.png' /></Col>
                      <Col lg='4' className='mb-3'><img alt='' className='mb-3' src='../../06-WeekSix/marking_punch.png' /></Col>
                      <Col lg='4' className='mb-3'><img alt='' className='mb-3' src='../../06-WeekSix/pin-loosening_punch.png' /></Col>
                    </Row>

                    <h5 className='mt-5'>Solder PCB with <a href='https://en.wikipedia.org/wiki/Flux_(metallurgy)'>Flux</a></h5>
                    <p>
                      It is also beneficial to use a liquid flux pen to put some flux on the board before soldering any SMD components.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' src='../../06-WeekSix/solder_flux.png' /></Col>
                    </Row>
                  </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>End Result</ListGroupItemHeading>
                    <p>
                      The board I redrew finally works after I programmed it. The ATtiny 412 programming process and the setup environment will be introduced in <Link to ='/week/8'>Week 8: Embedded Programming</Link>.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><h6>Blink the LED</h6>
                      <video width='100%' controls>
                        <source src="../../06-WeekSix/ATtiny412_blink-small.mp4" type="video/mp4" />
                      </video></Col>
                      <Col lg='6' className='mb-3'><h6>Blink the LED if the button is pressed</h6>
                      <video width='100%' controls>
                        <source src="../../06-WeekSix/ATtiny412_button-small.mp4" type="video/mp4" />
                      </video></Col>
                    </Row>
                  </ListGroupItem>
                  <ListGroupItem action>
                    <ListGroupItemHeading>Problem & Solution</ListGroupItemHeading>
                    <p>
                      The inital connection of the switch I designed was wrong, which I found out after uploading the code to the board. It lacked a <code>10kΩ resistor</code> as a pull-up or pull-down resistor whose function can be found from the <a href='https://learn.sparkfun.com/tutorials/pull-up-resistors/all'>Sparkfun: Pull-up Resistors</a> article.
                      Since it takes quite a long time to reconstruct a new board, I then decided to solder a through-hole resistor to fix the problem.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><img alt='' className='mb-2' src='../../06-WeekSix/Sch_SW_wrong.png' /><img alt='' src='../../06-WeekSix/hero-shot_no_R.jpg' /></Col>
                      <Col lg='6' className='mb-3'><img alt='' className='mb-2' src='../../06-WeekSix/Sch_SW_correct.png' /><img alt='' src='../../06-WeekSix/hero-shot_add_R.jpg' /></Col>
                    </Row>
                    <p>
                      It works after I reuploaded the code.
                    </p>
                    <Row className='mb-3'>
                      <Col lg='6' className='mb-3'><h6>LED OFF</h6><img alt='' src='../../06-WeekSix/hero-shot_btn.jpg' /></Col>
                      <Col lg='6' className='mb-3'><h6>Press the button to light up LED</h6><img alt='' src='../../06-WeekSix/hero-shot_press_btn.jpg' /></Col>
                    </Row>
                  </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
        <hr />
        <Container>
          <Row>
            <Col className='h3'>Group Assignment: Debugging & Measurement</Col>
          </Row>
          <Row>
            <Col>
              <ListGroup>
                <ListGroupItem action>
                  <ListGroupItemHeading>Debugging Tool: <a href='https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter/all'>Multimeter</a></ListGroupItemHeading>
                  <p>
                    After finishing soldering, I used a multimeter and switched it to the <code>buzzing mode</code> which sounds if two points are connected (<code>0Ω</code> between them).
                    In order to find out how much current does each LED consume, I then measured their cross voltage by switching to the <code>DC voltage</code> mode:
                  </p>
                  <Row className='mb-3'>
                    <Col lg='6' className='mb-3'><h6>Measure the Connection</h6>
                    <video width='100%' controls>
                      <source src="../../06-WeekSix/PCB_measure_R-small.mp4" type="video/mp4" />
                    </video></Col>
                    <Col lg='6' className='mb-3'><h6>Measure the Cross Voltage</h6>
                    <video width='100%' controls>
                      <source src="../../06-WeekSix/PCB_measure_V-small.mp4" type="video/mp4" />
                    </video></Col>
                  </Row>
                  <p>
                    The red dots between each component are multimeter probes. According to the statistic, I can calculate the exact current which go through both the LED and the resistor:
                  </p>
                  <pre>
                    <h6>
                      Voltage across a resistor (volts) = Current through the resistor (amperes)* The Resistance of the resistor (ohms)
                    </h6>
                    <h6 className='font-weight-bold'>
                      → Vcc - V<sub>F</sub> = I<sub>F</sub> * R
                    </h6>
                    <h6 className='font-weight-bold'>
                      → V<sub>R</sub> = I<sub>F</sub> * R
                    </h6>
                    <h6 className='font-weight-bold'>
                      → 2.148V = I<sub>F</sub> * 499 Ω
                    </h6>
                    <h6 className='font-weight-bold'>
                      → I<sub>F</sub> = 0.0043A = 4.3mA
                    </h6>
                </pre>
                  <Row className='mt-5 mb-3'>
                    <Col lg='6' className='mb-3'><h6>V<sub>F</sub>: Cross Voltage of Power LED</h6><img alt='' className='mb-2' src='../../06-WeekSix/Sch_pwr_LED_V_measure.png' /><a href='../../06-WeekSix/PCB_measure_pwr_LED_V.png'><img alt='' src='../../06-WeekSix/PCB_measure_pwr_LED_V.png' /></a></Col>
                    <Col lg='6' className='mb-3'><h6>V<sub>R</sub>: Cross Voltage of Resistor</h6><img alt='' className='mb-2' src='../../06-WeekSix/Sch_pwr_LED_R_V_measure.png' /><a href='../../06-WeekSix/PCB_measure_pwr_LED_R_V.png'><img alt='' src='../../06-WeekSix/PCB_measure_pwr_LED_R_V.png' /></a></Col>
                  </Row>
                  <p>
                    Then I used the multimeter to verify my calculation and it seems that there is some difference between the theoretical number and the practical one when it comes to measuring small current which can be observed by an <a href='https://learn.sparkfun.com/tutorials/how-to-use-an-oscilloscope/all'>oscilloscope</a> in a more precise way:
                  </p>
                  <h6>LED OFF</h6>
                  <img alt='' className='mb-3' src='../../06-WeekSix/PCB_measure_pwr_LED_I_off.jpg' />
                  <h6>LED ON</h6>
                  <img alt='' className='mb-3' src='../../06-WeekSix/PCB_measure_pwr_LED_I_on.jpg' />
                </ListGroupItem>
              </ListGroup>
            </Col>
          </Row>
        </Container>
      </div>
    </Layout>
  );
}

