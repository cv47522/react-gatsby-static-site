export const dependencies =
  `"dependencies": {
    "bootstrap": "^4.4.1",
    "jquery": "^3.4.1",
    "react": "^16.12.0",
    "react-dom": "^16.12.0",
    "react-router-dom": "^5.1.2",
    "reactstrap": "^8.4.1"
  }`;

export const devDpendencies =
  `"devDependencies": {
    "@babel/core": "^7.8.4",
    "@babel/plugin-proposal-class-properties": "^7.8.3",
    "@babel/plugin-proposal-object-rest-spread": "^7.8.3",
    "@babel/preset-env": "^7.8.4",
    "@babel/preset-react": "^7.8.3",
    "babel-loader": "^8.0.6",
    "compression-webpack-plugin": "^3.1.0",
    "css-loader": "^3.4.2",
    "style-loader": "^1.1.3",
    "webpack": "^4.41.5",
    "webpack-bundle-analyzer": "^3.6.0",
    "webpack-cli": "^3.3.10",
    "webpack-dev-server": "^3.10.2"
  }`;

export const scripts = `
  "scripts": {
    "build": "webpack",
    "watch": "webpack -w",
    "start": "webpack-dev-server",
  }`;

export const webpackConfig = `
  const path = require('path');
  const webpack = require('webpack');
  const srcPath = path.resolve(__dirname, 'src');
  const distPath = path.resolve(__dirname, 'dist');

  module.exports = {
    // mode: 'development',
    mode: 'production',
    context: srcPath,
    resolve: {
      alias: {
        components: path.resolve(srcPath, 'components'),
        api: path.resolve(srcPath, 'api'),
        images: path.resolve(srcPath, 'images'),
      },
    },
    entry: {
      index: './index.jsx',
      vendor: ['react', 'react-dom'],
    },
    output: {
      path: distPath,
      filename: '[name].bundle.js',
      publicPath: '/',
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: [/node_modules/],
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env', '@babel/preset-react'],
                plugins: [
                  '@babel/plugin-proposal-class-properties',
                  '@babel/plugin-proposal-object-rest-spread',
                ],
              },
            },
          ],
        },
        {
          test: /\.css$/i,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                url: false,
              },
            },
          ],
        },
        {
          test: /\.(png|jpg|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192,
              },
            },
          ],
        },
      ],
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all',
          },
        },
      },
    },
    devServer: {
      historyApiFallback: true,
      hot: true,
      inline: true,
      contentBase: distPath,
      compress: true,
      port: 6060,
    },
    devtool: 'source-map',
  };`;

export const gitlabCICD =
  `# Using the latest node version provided by Docker images to build the React app.
  image: node:latest

# Cache node modules to speed up future builds.
  cache:
    paths:
    - node_modules

# Name the stages involved in the pipeline.
# Specify the stages. Default stages of a pipeline are: build, test and deploy.
# Order matters.
  stages:
    - deploy

# Job name for gitlab to recognise this results in assets for Gitlab Pages
  pages:
    stage: deploy

# The first two lines are the same scripts that I run locally to build the final bundle files.
    script:
      - npm install       # Install all dependencies.
      - npm run build     # Build for production.
      - rm -rf public     # Remove the public folder built last time.
      - mv dist public    # Move built files from the "dist" folder to the "public" folder since Gitlab Pages works only with "public" directory.

    artifacts:
      paths:
      - public  # The built files in the "public" folder for Gitlab Pages to serve.

    only:
      - master # Only run on master branch.`;
