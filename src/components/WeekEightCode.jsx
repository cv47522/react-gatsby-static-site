export const attiny412_blink =
`#include <avr/io.h>
#define LED_AVR PIN3_bm //PA3:LED pin

void setup() {
  PORTA.DIRSET = LED_AVR;
}

void loop() {
  PORTA.OUT |= LED_AVR;
  delay(100);
  PORTA.OUT &= ~LED_AVR;
  delay(100);
}`;

export const attiny412_blink_simplified =
`#define LED_AVR 4 //PA3:LED pin

void setup() {
  pinMode(LED_AVR, OUTPUT)
}

void loop() {
  digitalWrite(LED_AVR, HIGH);
  delay(100);
  digitalWrite(LED_AVR, LOW);
  delay(100);
}`;

export const attiny412_echo =
`#define max_buffer 25

static int index = 0;
static char chr;
static char buffer[max_buffer] = {0};

void setup() {
  Serial.begin(115200);
}

void loop() {
  if (Serial.available() > 0) {
    chr = Serial.read();
    Serial.print("hello.t412.echo: you typed \"");
    buffer[index++] = chr;
    if (index == (max_buffer-1))
        index = 0;
    Serial.print(buffer);
    Serial.println("\"");
    }
  }`;

export const scripts = `
  "scripts": {
    "build": "webpack",
    "watch": "webpack -w",
    "start": "webpack-dev-server",
  }`;

export const webpackConfig = `
  const path = require('path');
  const webpack = require('webpack');
  const srcPath = path.resolve(__dirname, 'src');
  const distPath = path.resolve(__dirname, 'dist');

  module.exports = {
    // mode: 'development',
    mode: 'production',
    context: srcPath,
    resolve: {
      alias: {
        components: path.resolve(srcPath, 'components'),
        api: path.resolve(srcPath, 'api'),
        images: path.resolve(srcPath, 'images'),
      },
    },
    entry: {
      index: './index.jsx',
      vendor: ['react', 'react-dom'],
    },
    output: {
      path: distPath,
      filename: '[name].bundle.js',
      publicPath: '/',
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: [/node_modules/],
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env', '@babel/preset-react'],
                plugins: [
                  '@babel/plugin-proposal-class-properties',
                  '@babel/plugin-proposal-object-rest-spread',
                ],
              },
            },
          ],
        },
        {
          test: /\.css$/i,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                url: false,
              },
            },
          ],
        },
        {
          test: /\.(png|jpg|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192,
              },
            },
          ],
        },
      ],
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all',
          },
        },
      },
    },
    devServer: {
      historyApiFallback: true,
      hot: true,
      inline: true,
      contentBase: distPath,
      compress: true,
      port: 6060,
    },
    devtool: 'source-map',
  };`;

export const gitlabCICD =
  `# Using the latest node version provided by Docker images to build the React app.
  image: node:latest

# Cache node modules to speed up future builds.
  cache:
    paths:
    - node_modules

# Name the stages involved in the pipeline.
# Specify the stages. Default stages of a pipeline are: build, test and deploy.
# Order matters.
  stages:
    - deploy

# Job name for gitlab to recognise this results in assets for Gitlab Pages
  pages:
    stage: deploy

# The first two lines are the same scripts that I run locally to build the final bundle files.
    script:
      - npm install       # Install all dependencies.
      - npm run build     # Build for production.
      - rm -rf public     # Remove the public folder built last time.
      - mv dist public    # Move built files from the "dist" folder to the "public" folder since Gitlab Pages works only with "public" directory.

    artifacts:
      paths:
      - public  # The built files in the "public" folder for Gitlab Pages to serve.

    only:
      - master # Only run on master branch.`;
